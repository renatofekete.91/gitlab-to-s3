import React from 'react'
import ReactDOM from 'react-dom'

function App() {
  return (
    <div>
      <h1>GITLAB + S3</h1>
    </div>
  )
}

ReactDOM.render(<App />, document.querySelector('#app'))
